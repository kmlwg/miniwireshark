#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>
#include <errno.h>

#include <pcap.h>

#include <sys/types.h>
#include <sys/socket.h>

// Packet headers structures
#include <net/ethernet.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <netinet/udp.h>
#include <netinet/ip.h>
#include <netinet/ip_icmp.h>
#include <arpa/inet.h>


// Default snap length (max bytes per packet to capture)
#define SNAP_LEN 1518

struct arphdr {
    unsigned short hrd;         // Hardware type
    unsigned short pro;         // Protocol type
    unsigned char  hln;         // Hardware adress length
    unsigned char  pln;         // Protocol adress length
    unsigned short op;          // Opcode

#define ARP_REQUEST 1
#define ARP_REPLY   2

    u_char sha[6];       // Sender hardware adress
    u_char spa[4];       // Sender ip adress
    u_char tha[6];       // Target hardware adress
    u_char tpa[4];       // Target ip adress
};

    
// Function prototypes
void got_packet(u_char *args, const struct pcap_pkthdr *header, const u_char *packet);
void print_ethernet_header(const u_char *packet);
void print_ip_header(const u_char *packet);
void print_tcp_packet(const u_char *packet, int size);
void print_udp_packet(const u_char *packet, int size);
void print_icmp_packet(const u_char *packet, int size);
void print_arp_header(const u_char *packet);
void print_data_dump();

int main(int argc, char* argv[])
{
    char        *dev = NULL;                    // Capture device name
    char        errbuff[PCAP_ERRBUF_SIZE];      // Error buffer
    pcap_t      *handle;                        // packet capture handle
    char        filter_exp[] = "ip or arp";     // Filter expression
    struct      bpf_program fp;                 // Compiled filter programm
    bpf_u_int32 mask;                           // Subnet mask
    bpf_u_int32 net;                            // IP
    int         num_packets = 30;               // Number of packets to captur

    // Check for capture device name from cmd-l
    if (argc == 2) {
        dev = argv[1];
    } else if (argc > 2) {
        fprintf(stderr, "Error: unrecognized command-line options\n\n");
        exit(EXIT_FAILURE);
    } else {
        // Find default capture device
        dev = pcap_lookupdev(errbuff);
        if (dev == NULL) {
            fprintf(stderr, "Couldn't find default device: %s\n", errbuff);
            exit(EXIT_FAILURE);
        }
    }

    // Get IP and mask of the capture device
    if (pcap_lookupnet(dev, &net, &mask, errbuff) == -1) {
        fprintf(stderr, "Couldn't get netmask for device %s: %s\n", dev, errbuff);
        net  = 0;
        mask = 0;
    }


    // Print capture infoS
    printf("Device: %s\n", dev);
    printf("    IP: %d\n", net);
    printf("    Mask: %d\n\n", mask);
    printf("Number of packets: %d\n", num_packets);
    printf("Filter expression: %s\n", filter_exp);

    // Open capture device
    handle = pcap_open_live(dev, SNAP_LEN, 1, 1000, errbuff);
    if (handle == NULL) {
        fprintf(stderr, "Couldn't open device %s: %s\n", dev, errbuff);
        exit(EXIT_FAILURE);
    }

    // Make sure capturinf is on Ethernet device
    if (pcap_datalink(handle) != DLT_EN10MB) {
        fprintf(stderr, "Device %s is not on Ethernet.\n", dev);
        exit(EXIT_FAILURE);
    }

    // Compile filter expression
    if (pcap_compile(handle, &fp, filter_exp, 0, net) == -1) {
        fprintf(stderr, "Couldn't parse filter %s: %s\n", 
            filter_exp, pcap_geterr(handle));
        exit(EXIT_FAILURE);
    }

    // Apply the compiled filter
    if (pcap_setfilter(handle, &fp) == -1) {
        fprintf(stderr, "Couldn't install filter %s: %s",
            filter_exp, pcap_geterr(handle));
        return(EXIT_FAILURE);
    }

    // Set callback function
    pcap_loop(handle, num_packets, got_packet, NULL);

    // Cleanup
    pcap_freecode(&fp);
    pcap_close(handle);

    printf("\nProcces complete\n");

    return(0);
}

void
got_packet(u_char *args, const struct pcap_pkthdr *header, const u_char *packet)
{
    static int count = 1;                   // Shared packet counter

    
    // Skip Ethernet header and read ip header
    const struct iphdr *ip = (struct iphdr*)(packet + sizeof(struct ethhdr));
    int size = header->len;

    switch (ip->protocol) {
        case IPPROTO_TCP:
            print_tcp_packet(packet, size);
            break;
        case IPPROTO_UDP:
            print_udp_packet(packet, size);
            break;
        case IPPROTO_ICMP:
            print_icmp_packet(packet, size);
            break;
        case IPPROTO_IP:
            printf("    Protocol: IP\n");
            print_ethernet_header(packet);
            print_ip_header(packet);
            break;
        default:
            print_arp_header(packet);
    }

    count++;
}

void
print_ethernet_header(const u_char *packet) {
    const struct ether_header *eth = (struct ether_header *)packet;
    
    printf("\n");
    printf("Ethernet header:\n");
    printf("   | Dest address: %.2X-%.2X-%.2X-%.2X-%.2X-%.2X\n", eth->ether_dhost[0], eth->ether_dhost[1],
            eth->ether_dhost[2], eth->ether_dhost[3], eth->ether_dhost[4], eth->ether_dhost[5]);
    printf("   | Src address:  %.2X-%.2X-%.2X-%.2X-%.2X-%.2X\n", eth->ether_shost[0], eth->ether_shost[1],
            eth->ether_shost[2], eth->ether_shost[3], eth->ether_shost[4], eth->ether_shost[5]);
    printf("   | Protcol:      %u\n", (unsigned short)eth->ether_type);
}

void 
print_ip_header(const u_char *packet) {
    const struct iphdr *ip = (struct iphdr*)(packet + sizeof(struct ethhdr));

    struct sockaddr_in source,dest;

    memset(&source, 0, sizeof(source));
	source.sin_addr.s_addr = ip->saddr;
	
	memset(&dest, 0, sizeof(dest));
	dest.sin_addr.s_addr = ip->daddr;

    printf("\n");
    printf("IP header\n");
    printf("   | IP ver.:          %d\n", ip->version);
    printf("   | IP header length: %d bytes\n", ip->ihl * 4);
    printf("   | Type of service:  %d\n",(unsigned int)ip->tos);
    printf("   | IP total length:  %d bytes\n",(unsigned int)ip->tot_len);
    printf("   | Identification:   %d\n", (unsigned int)ip->id);
    printf("   | Time to live:     %d\n", (unsigned int)ip->ttl);
    printf("   | Protocol:         %d\n", (unsigned int)ip->protocol);
    printf("   | Checksum:         %d\n", (unsigned int)ip->check);
    printf("   | Source IP:        %s\n", inet_ntoa(source.sin_addr));
    printf("   | Destination IP:   %s\n", inet_ntoa(dest.sin_addr));
}

void
print_tcp_packet(const u_char *packet, int size) {
    unsigned short iph_len;
    const struct iphdr *ip = (struct iphdr*)(packet + sizeof(struct ethhdr));
    iph_len = ip->ihl * 4;

    const struct tcphdr *tcp = (struct tcphdr *)(packet + iph_len + sizeof(struct ethhdr));
    int data_offset = sizeof(struct ethhdr) + iph_len + tcp->doff * 4;

    printf("\n\n================================= TCP PACKET ================================\n");
    print_ethernet_header(packet);
    print_ip_header(packet);
    
    printf("\n");
    printf("TCP header\n");
    printf("   | Source port:          %u\n", ntohs(tcp->source));
    printf("   | Destination port:     %u\n", ntohs(tcp->dest));
    printf("   | Sequence number:      %u\n", ntohl(tcp->seq));
    printf("   | Acknowledge number:   %u\n", ntohl(tcp->ack_seq));
    printf("   | Header length:        %d bytes\n", (unsigned int)tcp->doff * 4);
    printf("   | Urgent flag:          %d\n", (unsigned int)tcp->urg);
    printf("   | Acknowledgement flag: %d\n", (unsigned int)tcp->ack);
    printf("   | Push flag:            %d\n", (unsigned int)tcp->psh);
    printf("   | Reset flag:           %d\n", (unsigned int)tcp->rst);
    printf("   | Synchronise flag:     %d\n", (unsigned int)tcp->syn);
    printf("   | Finish flag:          %d\n", (unsigned int)tcp->fin);
    printf("   | Window:               %d\n", ntohs(tcp->window));
    printf("   | Checksum:             %d\n", ntohs(tcp->check));
    printf("   | Urgent pointer:       %d\n", tcp->urg_ptr);

    print_data_dump(tcp + data_offset, size - data_offset);
    printf("###############################################################################\n");
}

void
print_udp_packet(const u_char *packet, int size) {
    unsigned short iph_len;
    const struct iphdr *ip = (struct iphdr*)(packet + sizeof(struct ethhdr));
    iph_len = ip->ihl * 4;

    struct udphdr *udp = (struct udphdr *)(packet + sizeof(struct ethhdr) + iph_len);
    int data_offset = sizeof(struct ethhdr) + iph_len + sizeof(struct udphdr);

    printf("\n\n================================ UDP PACKET =================================\n");
    print_ethernet_header(packet);
    print_ip_header(packet);

    printf("\n");
    printf("UDP header\n");
    printf("   | Source port:      %d\n", ntohs(udp->source));
    printf("   | Destination port: %d\n", ntohs(udp->dest));
    printf("   | UDP length:       %d\n", ntohs(udp->len));
    printf("   | Checksum:         %d\n", ntohs(udp->check));
    
    print_data_dump(packet + data_offset, size - data_offset);
    printf("#############################################################################\n");
}

void
print_icmp_packet(const u_char *packet, int size) {
    unsigned short iph_len;
    const struct iphdr *ip = (struct iphdr*)(packet + sizeof(struct ethhdr));
    iph_len = ip->ihl * 4;

    const struct icmphdr *icmp = (struct icmphdr *)(packet + sizeof(struct ethhdr) + iph_len);
    int data_offset = sizeof(struct ethhdr) + iph_len + sizeof(struct icmphdr);

    printf("\n\n================================ ICMP PACKET ===============================\n");
    print_ethernet_header(packet);
    print_ip_header(packet);

    printf("\n");
    printf("ICMP header\n");
    printf("   | Type: %d\n", (unsigned int)(icmp->type));
    printf("   | Code: %d\n", (unsigned int)(icmp->code));
    printf("   | Checksum: %d\n", ntohs(icmp->checksum));


    print_data_dump(packet + data_offset, size - data_offset);
    printf("#############################################################################\n");
}

void
print_arp_header(const u_char *packet) {
    const struct arphdr *arp = (struct arphdr*)(packet + sizeof(struct ethhdr));

    printf("\n\n=============================== ARP PACKET ===============================\n");
    print_ethernet_header(packet);
    
    printf("\n");
    printf("ARP header\n");
    printf("   | Format or hardware sdress: %d\n", ntohs(arp->hrd));
    printf("   | Format or protocol adress: %d\n", ntohs(arp->pro));
    printf("   | Length of hardware adress: %d\n", (unsigned int)arp->hln);
    printf("   | Length of protocol adress: %d\n", (unsigned int)arp->pln);
    printf("   | ARP opcode:                %s\n", (unsigned int)arp->op==ARP_REQUEST ? "ARP request" : "ARP reply");
    printf("   | Sender hardware adress:     ");
    for(int i=0; i<6; i++) i<5 ? printf("%02X:",arp->sha[i]) : printf("%02X",arp->sha[i]);
    printf("\n");
    printf("   | Sender ip adress:           ");
    for(int i=0; i<4; i++) i<3 ? printf("%d.",arp->spa[i]) : printf("%d",arp->spa[i]);
    printf("\n");
    printf("   | Target hardware adress:     ");
    for(int i=0; i<6; i++) i<5 ? printf("%02X:",arp->tha[i]) : printf("%02X",arp->tha[i]);
    printf("\n");
    printf("   | Target ip adress:           ");
    for(int i=0; i<4; i++) i<3 ? printf("%d.",arp->tpa[i]) : printf("%d",arp->tpa[i]);
    printf("\n");
}

void 
print_data_dump(const u_char *data, int size) {
    for (int i = 0; i < size; i++) {
        if ( i != 0 && i % 16 == 0) {
            printf("          ");
            for (int j = i - 16; j < i; j++) {
                if (data[j] >= 32 && data[j] <= 128) {
                    printf("%c",(unsigned char)data[j]);    // if number or char print
                } else {
                    printf(".");    // else .
                }
            }
            printf("\n");
        }
        if (i % 16 == 0) {
            printf("   ");
        }
        printf(" %02X", (unsigned int)data[i]);

        if (i == size - 1) {
            for (int j = 0; j < 15 - i % 16; j++) {
                printf("   ");
            }
            printf("          ");

            for (int j = i - i % 16; j <= i; j++) {
                if (data[j] >= 32 && data[j] <= 128) {
                    printf("%c", (unsigned char)data[j]);
                } else {
                    printf(".");
                }
            }
            printf("\n");
        }
    }

}
