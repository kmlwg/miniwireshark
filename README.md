# MiniWireShark

Command line mini wireshark written in C using libpcap.

Compile on linux using: gcc -Wall -pedantic sniffer.c -o sniffer.out -lpcap
Must run as super user.